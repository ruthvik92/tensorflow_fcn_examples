# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is repo for implementing backprop on fully connected networks with tensorflow, individual notebooks have variations in terms of activation functions, cost functions, input data pipeline etc
* Version 1.0
* Important links are embedded within the notebooks.

### Observations ###
* For a smaller dataset like MNIST using either tf.data API or using feed dict methods to feed data to the model doesn't
matter but for larger datasets you'll see drastic improvements.
* See [this link](https://dominikschmidt.xyz/tensorflow-data-pipeline/) for a nice diagramatic differenece between feed
dict and tf.data APIs
### Contribution guidelines ###

* Code review: Let me know if you want to add anything else or any mistakes.


### Who do I talk to? ###

* Ruthvik Vaila
* ruthvik.nitc@gmail.com
